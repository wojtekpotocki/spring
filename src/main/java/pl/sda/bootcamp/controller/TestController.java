package pl.sda.bootcamp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.sda.bootcamp.model.Message;

import java.util.ArrayList;
import java.util.List;

@Controller
public class TestController {
    private final List<Message> messages = new ArrayList<>();

    @ResponseBody
    @RequestMapping(value = "/api/messages", method = RequestMethod.POST)
    public void createMessage(@RequestBody final Message message) {
        messages.add(message);
    }
}
