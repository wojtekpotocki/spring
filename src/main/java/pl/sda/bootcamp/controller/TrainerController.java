package pl.sda.bootcamp.controller;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/panel-trenera")
public class TrainerController {

    @GetMapping("")
    public String dashboard() {
        return "trainer/dashboard";
    }
}
