package pl.sda.bootcamp.controller;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.sda.bootcamp.model.Course;
import pl.sda.bootcamp.model.Mode;
import pl.sda.bootcamp.service.CourseService;
import pl.sda.bootcamp.service.UserService;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@Controller
@AllArgsConstructor
@RequestMapping(value = "/kurs")
public class CourseController {

    private final CourseService courseService;
    private final UserService userService;

    @GetMapping("/lista")
    public String list(@RequestParam(name = "idKursu", required = false) Integer id,
                       Model model) {
        model.addAttribute("id", id);
        model.addAttribute("courseList", courseService.findAll());
        return "course/list";
    }

    @GetMapping("/dodaj")
    public String add(Model model) {
        String[] cities =new String[]{"Warszawa", "Szczecin", "Gdańsk"};
        model.addAttribute("cities", cities);
        model.addAttribute("modeList", Mode.values());
        model.addAttribute("course", Course.builder().build());
        model.addAttribute("trainerList", userService.findUserByRoleName("trainer"));
        return "course/add";
    }

    @PostMapping("/dodaj")
    public String create(@ModelAttribute Course course, Model model) {
        String[] cities =new String[]{"Warszawa", "Szczecin", "Gdańsk"};
        model.addAttribute("cities", cities);
        model.addAttribute("course", Course.builder().build());
        model.addAttribute("createdCourse", course);
        System.out.println(course);
        courseService.save(course);
        return "course/add";
    }
}
