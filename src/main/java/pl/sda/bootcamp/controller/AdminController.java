package pl.sda.bootcamp.controller;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.sda.bootcamp.model.Role;
import pl.sda.bootcamp.model.User;
import pl.sda.bootcamp.service.CourseService;
import pl.sda.bootcamp.service.RoleService;
import pl.sda.bootcamp.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@AllArgsConstructor
@RequestMapping(value = "/admin")
public class AdminController {

    private final UserService userService;
    private final CourseService courseService;
    private final RoleService roleService;

    @GetMapping("/user/list")
    public String userList(Model model) {
        model.addAttribute("userList", userService.findAll());
        return "admin/user-list";
    }

    @GetMapping("/user/delete/{id}")
    public String userDelete(@PathVariable("id") Long id) {
        userService.delete(id);
        return "redirect:/admin/user/list";
    }

    @GetMapping("/user/edit/{id}")
    public String userEdit(@PathVariable("id") Long id, Model model,
                           final HttpSession session) {
        User user = userService.find(id);
        session.setAttribute("user_id", user.getId());
        session.setAttribute("user_role", user.getRole());

        model.addAttribute("user", user);
        model.addAttribute("courseList", courseService.findAll());

        return "admin/user-edit";
    }

    @PostMapping("/user/edit/{id}")
    public String userEdited(@PathVariable("id") Long id,
                             @ModelAttribute User user,
                             final HttpServletRequest httpServletRequest) {
        if ((Long) httpServletRequest.getSession().getAttribute("user_id") == id) {
            user.setId(id);
            Role role = (Role) httpServletRequest.getSession().getAttribute("user_role");
            user.setRole(role);
            if ("user".equals(role.getRoleName())) {
                userService.save(user);
            } else {
                userService.updateTrainer(user);
            }

        }
        httpServletRequest.getSession().invalidate();
        return "redirect:/admin/user/list";
    }

    @GetMapping("/trainer/dodaj")
    public String create(Model model) {
        model.addAttribute("trainer", User.builder().build());
        return "trainer/create";
    }

    @PostMapping("/trainer/dodaj/podsumowanie")
    public String summary(@Valid @ModelAttribute("trainer") User trainer,
                          BindingResult bindingResult,
                          Model model) {
        //bindingResult.rejectValue("field_name", "errorCode", "defaultMessage");
        // bindingResult.rejectValue("password", "trainer.password", "hasła są różne");
        if (bindingResult.hasErrors()) {
            model.addAttribute("trainer", trainer);
            bindingResult.getAllErrors().forEach(objectError -> System.out.println(objectError.getDefaultMessage()));
            return "trainer/create";
        }

        trainer.setRole(roleService.findByName("trainer"));
        userService.save(trainer);
        return "trainer/summary";
    }
}
