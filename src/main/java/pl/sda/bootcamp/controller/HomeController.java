package pl.sda.bootcamp.controller;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.sda.bootcamp.service.CourseService;

@Controller
@AllArgsConstructor
@RequestMapping(value = "/")
public class HomeController {

    private final CourseService courseService;

    @GetMapping
    public String home(Model model) {
        model.addAttribute("courseList", courseService.findAll());
        return "course/list";
    }

    @GetMapping("strona-glowna")
    public String homePage() {
        return "forward:kurs/lista";
    }
}
