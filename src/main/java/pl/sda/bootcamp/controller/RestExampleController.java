package pl.sda.bootcamp.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.sda.bootcamp.model.Message;

@Controller
public class RestExampleController {

    @GetMapping("/rest/hi")
    public ResponseEntity<String> sayHi() {
        return ResponseEntity.ok().body("Hi!");
    }

    @GetMapping("/rest/hello")
    public ResponseEntity<Message> sayHello() {
        return ResponseEntity.ok().body(new Message("Hello world!"));
    }

    @GetMapping("/rest/message")
    @ResponseBody
    public Message getMessage() {
        return new Message("Witaj świecie");
    }

    @ResponseBody
    @ResponseStatus()
    @PostMapping("/rest/message")
    public void createMessage(@RequestBody final Message message) {
        if (message.getText().equals("test")) {
            throw new IllegalArgumentException("Błędny komunikat");
        }
        System.out.println(message.getText());
    }
}
