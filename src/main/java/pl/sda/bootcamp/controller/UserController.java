package pl.sda.bootcamp.controller;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.sda.bootcamp.model.Course;
import pl.sda.bootcamp.model.User;
import pl.sda.bootcamp.service.CourseService;
import pl.sda.bootcamp.service.RoleService;
import pl.sda.bootcamp.service.UserService;

import java.util.List;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Controller
@AllArgsConstructor
@RequestMapping(value = "/uzytkownicy")
public class UserController {

    private final UserService userService;
    private final CourseService courseService;
    private final RoleService roleService;

    @GetMapping("/dodaj/{courseId}")
    public String register(@PathVariable Long courseId,
                           Model model) {
        model.addAttribute("user", User.builder().build());
        model.addAttribute("course", courseService.findById(courseId));
        //model.addAttribute("coursesList", courseService.findAll());
        return "user/register";
    }

    @PostMapping("/dodaj/{courseId}/podsumowanie")
    public String summary(@PathVariable Long courseId,
                          @ModelAttribute User user) {
        String email = user.getEmail();
        User existUser = userService.findByEmail(email);
        if (isNull(existUser)) {
            user.setRole(roleService.findByName("user"));
            userService.save(user);
        } else {
            Course course = courseService.findById(courseId);
            if (!existUser.getCourses().contains(course)) {
                existUser.getCourses().add(course);
                userService.save(existUser);
            }
        }
        return "user/summary";
    }
}
