package pl.sda.bootcamp.controller.api;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import pl.sda.bootcamp.model.UserRest;

import java.util.List;

@RestController
public class RestTemplateExampleController {

    @GetMapping("/rest-template/get")
    public UserRest[] getUsers() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<UserRest[]> exchange = restTemplate.exchange(
                "https://jsonplaceholder.typicode.com/users",
                HttpMethod.GET,
                HttpEntity.EMPTY,
                UserRest[].class
        );

        return exchange.getBody();
    }

    @GetMapping("/rest-template/get/list")
    public List<UserRest> getUserList() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List<UserRest>> exchange = restTemplate.exchange(
                "https://jsonplaceholder.typicode.com/users",
                HttpMethod.GET,
                HttpEntity.EMPTY,
                new ParameterizedTypeReference<List<UserRest>>() {}
        );

        return exchange.getBody();
    }

}
