package pl.sda.bootcamp.controller.api;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import pl.sda.bootcamp.model.Post;
import pl.sda.bootcamp.service.PostService;
import pl.sda.bootcamp.service.rest.PostRestService;

import java.util.List;

@Controller
@AllArgsConstructor
public class PostController {

    private final PostService postService;
    private final PostRestService postRestService;

    @GetMapping("/api/rest-template/post/all")
    @ResponseBody
    public List<Post> getAllPost() {
        return postService.getPosts();
    }

    @GetMapping("/api/rest-template/post/create")
    public void createPost() {
        postRestService.addPost();
        // tutaj do obsługi formularza
    }
}
