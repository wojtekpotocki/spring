package pl.sda.bootcamp.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.sda.bootcamp.model.Course;
import pl.sda.bootcamp.model.Mode;
import pl.sda.bootcamp.model.User;

import java.time.LocalDate;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CourseDto {
    private Long id;
    private String name;
    private String city;
    private Mode mode;
    private LocalDate startDate;
    private LocalDate endDate;
    private Integer price;
    private UserDto trainer;
    private List<UserDto> courseUsers;
}
