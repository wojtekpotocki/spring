package pl.sda.bootcamp.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.bootcamp.model.Course;
import pl.sda.bootcamp.model.User;
import pl.sda.bootcamp.repository.CourseRepository;
import pl.sda.bootcamp.repository.UserRepository;

import java.util.List;

@Service
@AllArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final CourseRepository courseRepository;

    public void save(User user) {
        userRepository.save(user);
    }

    @Transactional
    public void updateTrainer(User user) {
        courseRepository.findAll().forEach( course -> {
            if (user.getTrainerCourses().contains(course)) {
                course.setTrainer(user);
            } else if (course.getTrainer() != null && user.getId() == course.getTrainer().getId()) {
                course.setTrainer(null);
            }
            courseRepository.save(course);
        });
        this.save(user);
    }

    @Transactional
    public void delete(Long id) {
        List<Course> courseList = courseRepository.findByTrainer_Id(id);
        courseList.forEach(course -> {
            course.setTrainer(null);
            courseRepository.save(course);
        });
        userRepository.deleteById(id);
    }

    public User find(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Nie znaleziono usera o id: " + id));
    }

    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public List<User> findUserByRoleName(String roleName) {
        return userRepository.findByRole_RoleNameContains(roleName);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }
}
