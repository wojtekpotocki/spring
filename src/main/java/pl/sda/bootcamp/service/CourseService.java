package pl.sda.bootcamp.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.sda.bootcamp.model.Course;
import pl.sda.bootcamp.repository.CourseRepository;

import java.util.List;

@Service
@AllArgsConstructor
public class CourseService {

    private final CourseRepository courseRepository;

    public void save(Course course) {
        courseRepository.save(course);
    }


    public List<Course> findAll() {
        return courseRepository.findAll();
    }

    public Course find(Long id) {
        return courseRepository.find(id);
    }

    public Course findById(Long id) {
        return courseRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Nie znaleziono kursu o id: " + id));
    }
}
