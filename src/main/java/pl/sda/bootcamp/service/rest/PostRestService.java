package pl.sda.bootcamp.service.rest;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.sda.bootcamp.model.PostRest;

import java.util.List;

@Service
public class PostRestService {

    public List<PostRest> getPosts() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List<PostRest>> exchange = restTemplate.exchange(
                "https://jsonplaceholder.typicode.com/posts",
                HttpMethod.GET,
                HttpEntity.EMPTY,
                new ParameterizedTypeReference<List<PostRest>>() {}
        );

        return exchange.getBody();
    }

    public void  addPost() {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        PostRest postRest = PostRest.builder().title("test").body("test body").userId(2L).build();

        HttpEntity entity = new HttpEntity(postRest, headers);

        ResponseEntity<PostRest> exchange = restTemplate.exchange(
                "https://jsonplaceholder.typicode.com/posts",
                HttpMethod.POST,
                entity,
                PostRest.class);
    }
}










