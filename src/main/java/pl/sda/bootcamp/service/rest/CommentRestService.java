package pl.sda.bootcamp.service.rest;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.sda.bootcamp.model.CommentRest;

import java.util.List;

@Service
public class CommentRestService {

    public List<CommentRest> getComments() {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<List<CommentRest>> exchange = restTemplate.exchange(
                "https://jsonplaceholder.typicode.com/comments",
                HttpMethod.GET,
                HttpEntity.EMPTY,
                new ParameterizedTypeReference<List<CommentRest>>() {}
        );

        return exchange.getBody();
    }
}
