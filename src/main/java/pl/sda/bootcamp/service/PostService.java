package pl.sda.bootcamp.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.sda.bootcamp.factory.PostFactory;
import pl.sda.bootcamp.model.CommentRest;
import pl.sda.bootcamp.model.Post;
import pl.sda.bootcamp.model.PostRest;
import pl.sda.bootcamp.service.rest.CommentRestService;
import pl.sda.bootcamp.service.rest.PostRestService;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class PostService {

    private final PostRestService postRestService;
    private final CommentRestService commentRestService;
    private final PostFactory postFactory;

    public List<Post> getPosts() {
        List<PostRest> postRestList = postRestService.getPosts();
        List<CommentRest> commentRestList = commentRestService.getComments();
        return postRestList.stream()
                .map(post -> postFactory.create(post, commentRestList))
                .collect(Collectors.toList());
    }
}
