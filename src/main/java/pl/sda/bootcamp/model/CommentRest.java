package pl.sda.bootcamp.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommentRest {

    private Long postId;
    private Long id;
    private String name;
    private String email;
    private String body;
}
