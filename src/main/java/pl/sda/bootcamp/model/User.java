package pl.sda.bootcamp.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty(message = "Podaj imię")
    private String firstName;
    @NotEmpty(message = "{pl.sda.bootcamp.model.User.lastName.NotEmpty}")
    private String lastName;
    @NotEmpty
    @Column(unique = true)
    private String email;
    private String phone;

    @NotEmpty
    private String password;

    @Transient
    private String confirmPassword;

    @ManyToMany
//    @JoinTable(name = "user_courses",
//            joinColumns = @JoinColumn(name = "user_id"),
//            inverseJoinColumns = @JoinColumn(name = "courses_id"))
    private List<Course> courses;

    @OneToMany(mappedBy = "trainer")
    private List<Course> trainerCourses;

    @ManyToOne
    @JoinColumn(name = "role_id")
    private Role role;

    private Integer salary;
}
