package pl.sda.bootcamp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.sda.bootcamp.model.Course;

import java.util.List;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {
    List<Course> findByTrainer_Id(Long id);

    @Query(value = "SELECT distinct c FROM Course c LEFT JOIN FETCH c.user LEFT JOIN fetch c.trainer")
    List<Course> findAll();

    @Query(value = "SELECT c FROM Course c LEFT JOIN FETCH c.user LEFT JOIN fetch c.trainer WHERE c.id = :id")
    Course find(Long id);
}
