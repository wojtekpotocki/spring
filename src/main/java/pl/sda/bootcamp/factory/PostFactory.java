package pl.sda.bootcamp.factory;

import org.springframework.stereotype.Service;
import pl.sda.bootcamp.model.CommentRest;
import pl.sda.bootcamp.model.Post;
import pl.sda.bootcamp.model.PostRest;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PostFactory {

    public Post create(PostRest postRest, List<CommentRest> commentRests) {
        return Post.builder()
                .body(postRest.getBody())
                .id(postRest.getId())
                .title(postRest.getTitle())
                .userId(postRest.getUserId())
                .comments(extractComments(postRest.getId(), commentRests))
                .build();
    }

    private List<CommentRest> extractComments(Long postId, List<CommentRest> commentRests) {
        return commentRests.stream().filter(c -> c.getPostId() == postId).collect(Collectors.toList());
    }
}
